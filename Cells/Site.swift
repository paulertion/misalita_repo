//
//  Site.swift
//  Mi Salita
//
//  Created by Pablo  on 27/03/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Site : NSManagedObject {
    @NSManaged var nombre : String?
    @NSManaged var horario : String?
    @NSManaged var direccion : String?
    @NSManaged var telefono : String?
    @NSManaged var precio : String?
    @NSManaged var web : String?
    @NSManaged var imagen : NSData?
    @NSManaged var rating : String?

}
