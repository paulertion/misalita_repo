//
//  PlaceCell.swift
//  Mi Salita
//
//  Created by Pablo  on 21/03/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit

class PlaceCell: UITableViewCell {

    @IBOutlet var imagePlace: UIImageView!
    @IBOutlet var namePlace: UILabel!
    @IBOutlet var locationPlace: UILabel!
    @IBOutlet var timePlace: UILabel!
    
    
}
