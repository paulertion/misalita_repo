//
//  Place.swift
//  Mi Salita
//
//  Created by Pablo  on 21/03/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import Foundation
import UIKit


class Place {
    var nombre : String!
    var horario : String!
    var direccion : String!
    var telefono : String!
    var precio : String?
    var web : String?
    var imagen : UIImage?
    
    var rating : String! = "calificar"
    
    
    init(nombre : String, horario : String, direccion : String, telefono : String, precio : String, web : String, imagen : UIImage){
        self.nombre = nombre
        self.horario = horario
        self.direccion = direccion
        self.telefono = telefono
        self.precio = precio
        self.web = web
        self.imagen = imagen
       
    }
    
    var propietario:String!
    
    init(propietario:String){
        self.propietario = propietario
    }
    
}
