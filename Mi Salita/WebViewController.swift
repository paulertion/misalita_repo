//
//  WebViewController.swift
//  Mi Salita
//
//  Created by Pablo  on 26/03/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    @IBOutlet var vistaWeb: WKWebView!
    var nombreURL : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = URL(string: nombreURL) {
            let solicitud = URLRequest(url: url)
            self.vistaWeb.load(solicitud)
        }

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
