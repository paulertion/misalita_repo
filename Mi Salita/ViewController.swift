//
//  ViewController.swift
//  Mi Salita
//
//  Created by Pablo  on 21/03/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit
import MessageUI

class ViewController: UITableViewController {
// contiene (UIViewController, UITableViewDataSource, UITableViewDelegate)
    
    var lugares : [Place] = []
    
    var busquedaController : UISearchController!
    var resultadosBusqueda : [Place] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.busquedaController?.searchResultsUpdater = self // Implementamos los delegados al UIVC actual
        
    // Barra busqueda
    self.busquedaController = UISearchController(searchResultsController: nil)
    self.tableView.tableHeaderView = self.busquedaController!.searchBar
                                
        var sala = Place(nombre: "Sala Nuñez", horario: "Hasta las 00hs", direccion: "Ruiz Huidobro 2500, Nuñez, C1429 CABA", telefono: "1161549962", precio: "$90 la hora", web: "https://www.antiguacasanunez.com", imagen: #imageLiteral(resourceName: "sala dagobah"))
        lugares.append(sala)
        
        sala = Place(nombre: "Sala Azteca", horario: "Hasta las 23hs", direccion: "Avenida Triunvirato 3982, C1430 CABA", telefono: "1551359442", precio: "", web: "", imagen: #imageLiteral(resourceName: "sala azteca"))
        lugares.append(sala)
        
        sala = Place(nombre: "Sala El Palacio", horario: "Hasta las 23hs", direccion: "Av. Belgrano 2186, Monserrat", telefono: "49419800", precio: "", web: "", imagen: #imageLiteral(resourceName: "sala el palacio"))
        lugares.append(sala)
        
        sala = Place(nombre: "Sala Lalala", horario: "Hasta las 23hs", direccion: "Av. Donato Álvarez 1567, Villa Crespo", telefono: "", precio: "", web: "", imagen: #imageLiteral(resourceName: "sala lalala"))
        lugares.append(sala)
        
        sala = Place(nombre: "Sala San Telmo", horario: "Hasta las 23hs", direccion: "Piedras 770, San Telmo", telefono: "", precio: "", web: "", imagen: #imageLiteral(resourceName: "sala san telmo"))
        lugares.append(sala)
        
        sala = Place(nombre: "Sala Gallo", horario: "Hasta las 23hs", direccion: "Gallo 136, Once", telefono: "", precio: "", web: "", imagen: #imageLiteral(resourceName: "sala gallo"))
        lugares.append(sala)
        
        sala = Place(nombre: "Sala BigBang", horario: "Hasta las 23hs", direccion: "Sánchez 1925, Villa del Parque", telefono: "", precio: "", web: "", imagen: #imageLiteral(resourceName: "sala big bang"))
        lugares.append(sala)
        
        sala = Place(nombre: "Sala Palermo Vintage", horario: "Hasta las 23hs", direccion: "José A. Cabrera 5060, Palermo", telefono: "", precio: "", web: "", imagen: #imageLiteral(resourceName: "sala palermo vintage"))
        lugares.append(sala)
        
        // Configurar texto del boton BACK
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "Lugares", style: .plain, target: nil, action: nil)
        
        tableView.separatorColor = UIColor(displayP3Red: 0.5, green: 0.5, blue: 0.5, alpha: 0.2)
        // Alpha separador lugares
            
        }
    
        
//MARK: - UITableViewDataSource

override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}

// 3.a- MOSTRAR CANTIDAD DE CELDAS DE RESULTADOS
override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if self.busquedaController.isActive{
        return self.resultadosBusqueda.count
    } else {
        return self.lugares.count
    }
}

override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let sitios : Place!
    
    // 3.b- MOSTRAR CONTENIDO DE CELDAS DE RESULTADOS
    if self.busquedaController.isActive{
        sitios = self.resultadosBusqueda[indexPath.row]
    } else {
        sitios = self.lugares[indexPath.row]
    }
           
    let cell = tableView.dequeueReusableCell(withIdentifier: "SalasCell", for: indexPath) as! PlaceCell

            cell.imagePlace.image = sitios.imagen
            cell.namePlace.text = sitios.nombre
            cell.locationPlace.text = sitios.direccion
            cell.timePlace.text = sitios.horario
                   
           // imagenes redondeadas
           cell.imagePlace.layer.cornerRadius = 10.0
           cell.imagePlace.clipsToBounds = true
    
    return cell
}
    
//MARK: - Compartir
    
    // Funcion para compartir, eliminar, etc
           
       override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
               
        // Compartir
               
            let shareAction = UIContextualAction(style: .normal, title: "Compartir") { (action, sourceView, completionHandler) in
               
                // 3.c- MOSTRAR CONTENIDO DE COMPARTIR EN CELDAS DE RESULTADOS
                let sitios : Place
               
                if self.busquedaController.isActive{
                    sitios = self.resultadosBusqueda[indexPath.row]
                } else {
                    sitios = self.lugares[indexPath.row]
                }
               
               let shareDefaultText = "Quiero compartirte la sala de ensayo \(sitios.nombre!)"
               
               let shareImage = self.lugares[indexPath.row].imagen!
               
               let activityController = UIActivityViewController(activityItems: [shareDefaultText, shareImage], applicationActivities: nil)
               
                   
               self.present(activityController, animated: true, completion: nil)
               }
           
               // color de fondo del boton COMPARTIR
               shareAction.backgroundColor = UIColor(displayP3Red: 0.2, green: 0.2, blue: 0.9, alpha: 1.0)
               
        // Eliminar
               
               let deleteAction = UIContextualAction(style: .destructive, title: "Borrar") { (action, sourceView, completionHandler) in
                   self.lugares.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .fade)
               }
               
               // color de fondo del boton BORRAR
               deleteAction.backgroundColor = UIColor(displayP3Red: 0.9, green: 0.1, blue: 0.1, alpha: 1.0)
               
       
               // constante que contiene configuracion de BORRADO y COMPARTIR
               let swypeConfiguration = UISwipeActionsConfiguration(actions: [shareAction, deleteAction])
               
               return swypeConfiguration
    
               
       // Despues de configurar una celda, agregarla al ViewController desde la parte grafica
       // al data source y al delegate, arrastrandola.

       }

//MARK: - UITableViewDelegate


override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
}

//  SEGUE

override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showPlaceDetail"{
        if let filaElegida = self.tableView.indexPathForSelectedRow {  // fila seleccionada
            
            let destinationViewController = segue.destination as! DetailViewController
            destinationViewController.sala = self.lugares[filaElegida.row]
  //          destinationViewController.hidesBottomBarWhenPushed = true       // Ocultar TabBar
        }
        
    }
}
    
 // 1- FUNCION DE BUSQUEDA
    
    func filterContentFor(textToSearch: String) {
    
        self.resultadosBusqueda = self.lugares.filter({ (Place) -> Bool in
            let zonaToFind = Place.direccion.range(of: textToSearch, options: NSString.CompareOptions.caseInsensitive)
            return zonaToFind != nil
        })
    
    }


@IBAction func unwindAlControladorPrincipal (segue: UIStoryboardSegue) {
    
}

    
}   // Class


//MARK: - MessageUI

// El delegado avisa que el usuario terminó de escribir el SMS

extension ViewController : MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult){
        
        controller.dismiss(animated: true, completion: nil)     // Cerrar pagina sms
        print(result)
    }
}

// 2- FUNCION DE LA BUSQUEDA, PARA ACTUALIZAR MIENTRAS SE REALIZA

extension ViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let buscarTexto = busquedaController.searchBar.text {
            self.filterContentFor(textToSearch: buscarTexto)
            self.tableView.reloadData()
        }
    }
}
