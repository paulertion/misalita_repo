//
//  ReviewViewController.swift
//  07- MisRecetas
//
//  Created by Pablo  on 08/01/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {

    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var stackViewReview: UIStackView!
    
    var ratingSelected : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Stackview
        let scale = CGAffineTransform(scaleX: 0.0, y: 0.0)     //tamaño
        let translation = CGAffineTransform(translationX: 0.0, y: 500.0)    //posicion
        
        stackViewReview.transform = scale.concatenating(translation)
    }
    
    // Animar BOTONES de reseña
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        /*
         UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            self.stackViewReview.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: nil)
        */
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.5, options: [], animations: {
            self.stackViewReview.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: nil)
        
        
        
    }
    
    // Calificar sala
    
    @IBAction func ratingPressed(_ sender: UIButton) {
        
        switch sender.tag {
            case 1:
                ratingSelected = "no me gusta"
            case 2:
                ratingSelected = "neutral"
            case 3:
                ratingSelected = "me gusta"
            default:
                break
            }
    
    // Agregarle el RETURN SEGUE a los botones de calificar
    performSegue(withIdentifier: "unwindToDetailView", sender: sender)
        
        }
    
    
}
