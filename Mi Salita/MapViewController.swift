//
//  MapViewController.swift
//  MiSalita
//
//  Created by Pablo  on 11/03/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {
    
    var destino : Place!
    
    @IBOutlet var elMapa: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.elMapa.delegate = self     // Nos delegamos la config del mapa
        
        self.elMapa.showsCompass = true
        self.elMapa.showsBuildings = false
        self.elMapa.showsTraffic = true

        print("El mapa debe mostrar "+destino.nombre)
        
        // Buscar coordenadas desde direccion
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(destino.direccion) { (placemarks, error) in
            if error == nil {
                for placemark in placemarks! {      // Para "los lugares encontrados" en "lugares buscados"..
                    
                    let anotacion = MKPointAnnotation()     // "chincheta"
                    anotacion.title = self.destino.nombre
                    anotacion.subtitle = self.destino.horario
                    anotacion.coordinate = (placemark.location?.coordinate)!
                    
                    self.elMapa.showAnnotations([anotacion], animated: true)    // Mostrar chincheta en mapa
                    self.elMapa.selectAnnotation(anotacion, animated: true)     // Permite dejar seleccionar
                    
                    
                }
            } else {
                print("Hubo un error che: \(String(describing: error?.localizedDescription))")
            }
        }
        
        
        }
        
    /* Ocultar barra de estado
    func preferStatusBarHidden() -> Bool {
        return true
    } */
    
}
    
    
    // Agregar foto a ubicacion

    extension MapViewController : MKMapViewDelegate {
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            
            let identificador = "MyPin"
            
            if annotation.isKind(of: MKUserLocation.self) {     // Si el tipo de pin es de usuario, no hacer nada
                return nil
        }
            
            
            var vistaAnotacion : MKPinAnnotationView? = self.elMapa.dequeueReusableAnnotationView(withIdentifier: identificador) as? MKPinAnnotationView
            
            if vistaAnotacion == nil {      // Si no existe la vista..
                vistaAnotacion = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identificador)
                vistaAnotacion?.canShowCallout = true
            }
            
            // Asignamos y config imagen para mapa
            let imagenMapa = UIImageView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))
            imagenMapa.image = self.destino.imagen
            vistaAnotacion?.leftCalloutAccessoryView = imagenMapa
            
            // Cambiamos color al pincho
            vistaAnotacion?.pinTintColor = UIColor.blue
            
            return vistaAnotacion
        }
        
        
    }

