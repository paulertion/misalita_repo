//
//  DetailViewController.swift
//  07- MisRecetas
//
//  Created by Pablo  on 03/01/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit
import MessageUI
import SafariServices

class DetailViewController: UIViewController {
    
    @IBOutlet var imageDetail: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var ratingButton: UIButton!
    
    var sala : Place!
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.title = sala.nombre  //titulo en la vista, de la sala seleccionada
        
        self.imageDetail.image = self.sala.imagen   // asignamos imagen
        
        // cargar imagen de RATING antes de arrancar la app
        let imagen = UIImage(named: self.sala.rating)
        self.ratingButton.setImage(imagen, for: .normal)
        
        
       
// ajustar filas automaticamente a la fuente (esta hecho por VISTA)
        
        /*    self.tableView.estimatedRowHeight = 50.0
        self.tableView.rowHeight = UITableView.automaticDimension   */
        
    }
    

    //MARK: - Segue
    
    // SEGUE para dirigir al mapa segun direccion y lugar
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mostrarMapa"{
            let destinationViewController = segue.destination as! MapViewController
            destinationViewController.destino = self.sala
        }
    }
    
 
}   // Cierra clase

  //MARK: - UITableViewDataSource

extension DetailViewController : UITableViewDataSource {

func numberOfSections(in tableView: UITableView) -> Int {
    1
}

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    6
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceDetailCell", for: indexPath) as! PlaceDetailCell
    
    cell.backgroundColor = UIColor.clear    // Color de fondo de celdas: limpio
    
    switch indexPath.row {
        case 0:
            cell.keyLabel.text = "Nombre:"
            cell.valueLabel.text = self.sala.nombre
        case 1:
            cell.keyLabel.text = "Horario:"
            cell.valueLabel.text = self.sala.horario
        case 2:
            cell.keyLabel.text = "Dirección:"
            cell.valueLabel.text = self.sala.direccion
        case 3:
            cell.keyLabel.text = "Teléfono:"
            cell.valueLabel.text = self.sala.telefono
        case 4:
            cell.keyLabel.text = "Precio por hora:"
            cell.valueLabel.text = self.sala.precio
        case 5:
            cell.keyLabel.text = "Web:"
            cell.valueLabel.text = self.sala.web
        default:
            break
        }
    
    return cell
    }
    


// Despues de configurar una celda, agregarla al ViewController desde la parte grafica
// al data source y al delegate, arrastrandola.
    
    
    
    // Cerrar REVIEW y elegir imagen de calificacion de ReviewViewController
    
    @IBAction func close(segue: UIStoryboardSegue){
        
        if let reviewVC = segue.source as? ReviewViewController{
            
            if let rankeada = reviewVC.ratingSelected{                  // agarro la opcion toucheada
                self.sala.rating = rankeada                         // se la asigno a RATING
                let imagen = UIImage(named: self.sala.rating)       // agarro la IMAGEN de la opcion toucheada
                self.ratingButton.setImage(imagen, for: .normal)        // se la asigno al boton RATING
                
        /*      // NO FUNCIONA YA QUE NO TENGO UNA BASE CORE DATA CREADA EN LA ENTIDAD DE CLASE PLACE
                 
                if let contenedor = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer {
                let contexto = contenedor.viewContext
                    do {
                        try contexto.save()
                    } catch {
                        print("El error es el siguiente:")
                        print(error)
                    }
                }
        */
                
            }
                                
        }
            
    }
    
    
    
    // SEGUE DE REGRESO
    
    @IBAction func unwindAlDetailViewController (segue: UIStoryboardSegue) {
        
    }
    
    
}

    //MARK: - UITableViewDelegate

extension DetailViewController : UITableViewDelegate{
    
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
        switch indexPath.row {
        case 2:
            // Mostrar direccion en el MAPA
            self.performSegue(withIdentifier: "mostrarMapa", sender: nil)
        case 3:
            // LLAMAR y MENSAJEAR a la sala
            let alertControlador = UIAlertController(title: "Comunicarse con \(self.sala.nombre!)", message: "¿Querés enviar un mensaje o llamar a la sala?", preferredStyle: .actionSheet)
            
            // Mensaje:
            
            let smsAccion = UIAlertAction(title: "Mensaje", style: .default) { (action) in
                
                if MFMessageComposeViewController.canSendText() {       // Si se puede enviar sms (app configurada)
                    let msg = "¡Hola! Quiero consultar horario para reserva de sala"
                    let msgVC = MFMessageComposeViewController()
                    msgVC.body = msg
                    msgVC.recipients = [self.sala.telefono]
                    msgVC.messageComposeDelegate = self
                    
                    self.present(msgVC, animated: true, completion: nil)
                    
                }
            }
            
            alertControlador.addAction(smsAccion)
            
            // Llamar:
            
             let callAccion = UIAlertAction(title: "Llamar", style: .default) { (action) in
                
                if let URLTelefono = URL(string: "tel://\(self.sala.telefono!)"){
                    let app = UIApplication.shared
                    
                    if app.canOpenURL (URLTelefono){
                        app.open(URLTelefono, options: [:], completionHandler: nil)
                    }
                }
            }
            
            alertControlador.addAction(callAccion)
            
            // Cancelar:
            
            let smsCancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            alertControlador.addAction(smsCancel)
            
            
            self.present(alertControlador, animated: true, completion: nil)
            
            
            
            case 5:
            // Ingresar a la web:
            
                if let miURL = URL(string: self.sala.web!){
                
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = true
                
                let safariViewController = SFSafariViewController(url: miURL, configuration: config)
                present(safariViewController, animated: true, completion: nil)
            }
            
        default:
            break
        }
        
        tableView.deselectRow(at: indexPath, animated: true)    // quitar color de seleccion dsps de tocar fila
        
    }
    
        
}

//MARK: - MessageUI

// Nos delega la pantalla para ENVIAR MENSAJE

extension DetailViewController : MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult){
        
        controller.dismiss(animated: true, completion: nil)     // Cerrar pagina sms
        print(result)
    }
}
