//
//  YoMismoViewController.swift
//  Mi Salita
//
//  Created by Pablo  on 25/03/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit
import SafariServices

class YoMismoViewController: UITableViewController {
    
    let facebook = "https://wwww.facebook.com/mapplestoreba/"
    let instagram = "https://wwww.instagram.com/mapplestoreba/"
    let app = "https://apps.apple.com/ar/app/geekers/id1494235335"
    let reseña = "https://wwww.facebook.com/mapplestoreba/"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    /*
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tableView.dequeueReusableCell(withIdentifier: "AboutUsCell", for: indexPath)
            
            celda.backgroundColor = UIColor.clear
            
            switch indexPath.section{
            case 0:
                switch indexPath.row{
                case 0:
                    miCelda.text = "Valorarnos en la App Store"
                case 1:
                    miCelda.text = "Valoranos con un feedback"
                default:
                    break
                    }
            case 1:
                switch indexPath.row {
                case 0:
                    miCelda.text = "Facebook"
                case 1:
                    miCelda.text = "Instagram"
                default:
                    break
                }
                default:
                    break
            }

            
            return celda
        }
    */
    //MARK: - UITableViewDelegate

    
          
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                // UIApplicaction.shared (Abre safari)
                if let sitioWebURL = URL(string: self.app){
                let app = UIApplication.shared
                    if app.canOpenURL(sitioWebURL){
                        app.open(sitioWebURL, completionHandler: nil)
                    }
                }
            case 1:
                // MKWebView (Abre web por ViewController)
                performSegue(withIdentifier: "mostrarWebView", sender: self.reseña)
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                // SFSafariViewController   (Abre un Safari interno por Framework)
                if let miURL = URL(string: self.facebook){
                    
                    // Modo lectura en web (si esta disp)
                    let config = SFSafariViewController.Configuration()
                    config.entersReaderIfAvailable = true
                    
                    let safariViewController = SFSafariViewController(url: miURL, configuration: config)
                    present(safariViewController, animated: true, completion: nil)
                
                }
            case 1:
                
                if let sitioWebURL = URL(string: self.instagram){
                let app = UIApplication.shared
                    if app.canOpenURL(sitioWebURL){
                        app.open(sitioWebURL, completionHandler: nil)
                    }
                }
            default:
                break
            }
        default:
            break
        }
        
        self.tableView.deselectRow(at: indexPath, animated: true)   // Deseleccionar celda pulsada
          
      }
    
    
    // Enviar DATA (nombreURL) por SEGUE

        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mostrarWebView"{
            let destinoVC = segue.destination as! WebViewController
            destinoVC.nombreURL = sender as? String
        }
        
    }


}



