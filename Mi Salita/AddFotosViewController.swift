//
//  AddFotosViewController.swift
//  Mi Salita
//
//  Created by Pablo  on 21/03/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit
import Photos
import CoreData

class AddFotosViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    /*

    @IBOutlet var imageSala: UIImageView!
    @IBOutlet var textfieldNombre: UITextField!
    @IBOutlet var textfieldHorario: UITextField!
    @IBOutlet var textfieldDireccion: UITextField!
    @IBOutlet var textfieldTelefono: UITextField!
    @IBOutlet var textfieldPrecio: UITextField!
    @IBOutlet var textfieldWeb: UITextField!
    
    @IBOutlet var botonNoGusta: UIButton!
    @IBOutlet var botonGusta: UIButton!
    @IBOutlet var botonEncanta: UIButton!
    
    var rating : String?
    var lugares : Site?
    
    let defaultColor = UIColor(displayP3Red: 93.0/255.0, green: 189.0/255.0, blue: 168.0/255.0, alpha: 1.0)
    let selectedColor = UIColor.red
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.textfieldNombre.delegate = self
        self.textfieldHorario.delegate = self
        self.textfieldDireccion.delegate = self
        self.textfieldTelefono.delegate = self
        self.textfieldPrecio.delegate = self
        self.textfieldWeb.delegate = self

        pedirPermisosFotos()
       
    }
    
    @IBAction func botonGuardar(_ sender: Any) {
        
        
        if  textfieldNombre.text != "" && textfieldHorario.text != "" && textfieldDireccion.text != "" && textfieldTelefono.text != "" && textfieldPrecio.text != "" && textfieldWeb.text != "" && imageSala.image != nil {
        
        let nombre = self.textfieldNombre.text
        let horario = self.textfieldHorario.text
        let direccion = self.textfieldDireccion.text
        let telefono = self.textfieldTelefono.text
        let precio = self.textfieldPrecio.text
        let web = self.textfieldWeb.text
        let imagen = self.imageSala.image
        let rating = self.rating
            
            // Core Data (GUARDAR objeto)
            
            if let contenedor = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer {
            let contexto = contenedor.viewContext   // Se encarga de guardar toda la vista en CoreData
                
                self.lugares = NSEntityDescription.insertNewObject(forEntityName: "Salas", into: contexto) as? Site
                
                self.lugares?.nombre = nombre
                self.lugares?.horario = horario
                self.lugares?.direccion = direccion
                self.lugares?.telefono = telefono
                self.lugares?.precio = precio
                self.lugares?.web = web
                self.lugares?.imagen = imagen?.pngData() as NSData?
                self.lugares?.rating = rating
                
                do {
                    try contexto.save()
                } catch {
                    print("El error es el siguiente:")
                    print(error)
                }
                
            }
            performSegue(withIdentifier: "unwindToNuevas", sender: self)
            
            
        
        } else {
            
            let alertController = UIAlertController(title: "Faltan datos", message: "Chequea cada casilla y fijate cual quedó vacia de completar.", preferredStyle: .alert)
                              
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                              
            alertController.addAction(alertAction)
                              
            self.present(alertController, animated: true)
        }
        
    }
    
    @IBAction func botonRating(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            self.rating = "dislike"
            self.botonNoGusta.backgroundColor = selectedColor
            self.botonGusta.backgroundColor = defaultColor
            self.botonEncanta.backgroundColor = defaultColor
        case 2:
            self.rating = "good"
            self.botonNoGusta.backgroundColor = defaultColor
            self.botonGusta.backgroundColor = selectedColor
            self.botonEncanta.backgroundColor = defaultColor
        case 3:
            self.rating = "like"
            self.botonNoGusta.backgroundColor = defaultColor
            self.botonGusta.backgroundColor = defaultColor
            self.botonEncanta.backgroundColor = selectedColor
        default:
            break
        }
        
    }
    
    
    
    
    func pedirPermisosFotos() {
        PHPhotoLibrary.requestAuthorization { (estadoAutoriz) in
            
            DispatchQueue.main.async {              // Mover todo el codigo a tiempo real (principal)
                if estadoAutoriz != .authorized{
                    
                    self.navigationController?.popViewController(animated: true)
                    
                    /*
                    let alertController = UIAlertController(title: "Atención", message: "Nos has denegado el acceso a la libreria de fotos. Para continuar por favor autorizar el acceso desde los ajustes de la app en tu iPhone.", preferredStyle: .alert)
                                      
                    let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                      
                    alertController.addAction(alertAction)
                                      
                    self.present(alertController, animated: true)
                    
                    */
                    
                   
                               
                }
            }
        }
    }

    // MARK: - Table view data source

    /*
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
    */
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            // Hay que configurar la seleccion de la imagen del lugar (abrir fototeca)
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let tomarImagen = UIImagePickerController()
                tomarImagen.allowsEditing = false
                tomarImagen.sourceType = .photoLibrary
                tomarImagen.delegate = self     // Declaramos que somos los que llamamos al didFinishPickingMediaWithInfo
                
                self.present(tomarImagen, animated: true, completion: nil)
            }
            
        }
        
        
        
        // Deseleccionar celda
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    // Funcion para seleccionar imagen elegida
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.imageSala.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        self.imageSala.contentMode = .scaleToFill
        self.imageSala.clipsToBounds = true
        
        // Autolayouts por codigo
        let leadingConstraint = NSLayoutConstraint(item: self.imageSala!, attribute: .leading, relatedBy: .equal, toItem: self.imageSala.superview, attribute: .leading, multiplier: 1, constant: 0)
        leadingConstraint.isActive = true
        
        let trailingConstraint = NSLayoutConstraint(item: self.imageSala!, attribute: .trailing, relatedBy: .equal, toItem: self.imageSala.superview, attribute: .trailing, multiplier: 1, constant: 0)
        trailingConstraint.isActive = true
        
        let topConstraint = NSLayoutConstraint(item: self.imageSala!, attribute: .top, relatedBy: .equal, toItem: self.imageSala.superview, attribute: .top, multiplier: 1, constant: 0)
        topConstraint.isActive = true
        
        let bottomConstraint = NSLayoutConstraint(item: self.imageSala!, attribute: .bottom, relatedBy: .equal, toItem: self.imageSala.superview, attribute: .bottom, multiplier: 1, constant: 0)
        bottomConstraint.isActive = true
        
        
        dismiss(animated: true, completion: nil)
        // Y pedimos permisos en info.plis para acceder a la libreria
    }
    

}


extension UIViewController : UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}




/*
 
 if let nombre = self.textfieldNombre.text, let horario = self.textfieldHorario.text, let direccion = self.textfieldDireccion.text, let telefono = self.textfieldTelefono.text, let precio = self.textfieldPrecio.text, let web = self.textfieldWeb.text, let viewImagen = self.imageSala.image, let rating = self.rating
 */

*/

}
