//
//  NuevasViewController.swift
//  Mi Salita
//
//  Created by Pablo  on 27/03/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit
import CoreData
import CloudKit

class NuevasViewController: UITableViewController {
    
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    /*
    var lugares : [Site]?       // CoreData
    var resultadosFetch : NSFetchedResultsController<Site>!     // Search
    var sitios : [CKRecord] = []        // CloudKit

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Activity Indicator
        spinner.hidesWhenStopped = true
        spinner.center = self.view.center
        self.view.addSubview(spinner)
        spinner.startAnimating()
        
/*
        
        // Core Data (CARGAR objetos) [recuperacion de datos, con opciones: ordenar, actualizar]
        
        let solicitudFetch : NSFetchRequest<Site> = NSFetchRequest(entityName: "Salas") // El lugar en donde siempre esta mirando
        let descriptorOrden = NSSortDescriptor(key: "nombre", ascending: true)  // Ordena por NOMBRE
        solicitudFetch.sortDescriptors = [descriptorOrden]  // Podes agregar varias ordenadas (ej: ordenar x nombre)
        
        if let contenedor = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer {  // traemos el CONTENEDOR PERSISTENTE de appdelegate
        let contexto = contenedor.viewContext   // me quedo con el CONTEXTO del contenedor (se encarga de hacer modificaciones como borrado, insersion, etc)
            self.resultadosFetch = NSFetchedResultsController(fetchRequest: solicitudFetch, managedObjectContext: contexto, sectionNameKeyPath: nil, cacheName: nil)
            self.resultadosFetch.delegate = self    // Yo me encargo de los cambios realizados
            
            do {
                try resultadosFetch.performFetch()      // Intentar hacer la actualizacion
                self.lugares = resultadosFetch.fetchedObjects!      //  Agrega a LUGARES los objetos (salas) encontrados
            } catch {
                print("El error es el siguiente:")
                print(error)
            }
 
 
         }
 */
        
/*
        
        // Core Data (CARGAR objetos simple)

       if let contenedor = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer {
       let contexto = contenedor.viewContext
           
           let solicitud : NSFetchRequest<Site> = NSFetchRequest(entityName: "Salas")
           // let solicitud : NSFetchRequest<Place> = Place.fetchRequest()
           
           do {
               self.lugares = try contexto.fetch(solicitud)    // fetch request: solicitud de busqueda
               self.tableView.reloadData() // Recargar tabla
           } catch {
               print("El error es el siguiente:")
               print(error)
           }
       }
        
*/
        self.cargarDataDeiCloud()
        
    }   //VDL
    
    func cargarDataDeiCloud() {
        
        
        /* Descargar con la API Convenience (para POCA cantidad de datos)
               
               let iCloudContenedor = CKContainer.default()    // Contenedor basico en iCloud
               let publicDB = iCloudContenedor.publicCloudDatabase     // DataBase Publica
               let predicado = NSPredicate(value: true)    // Trae TODO el contenido (sin filtro)
               let query = CKQuery(recordType: "Salas", predicate: predicado)  // Hace la consulta en el Record: Salas
               
               publicDB.perform(query, inZoneWith: nil) { (results, error) in
                   if error != nil {
                       print(error as Any)
                       return
                   }
                   
                   if let resultados = results{
                       self.sitios = resultados
                       OperationQueue.main.addOperation {      // Realiza la carga de datos en primer plano en lugar de en 2do
                            self.spinner.stopAnimating()    // Detiene el spinner
                            self.tableView.reloadData()
                       }
                   }
               }
        */
        
        
         
        // LAZY LOADING
        // Descargar con la API Operacional (para GRAN cantidad de datos)
        // 1/2: Se descargan primero los datos STRING, y en un 2do hilo de ejecucion las imagenes (ir al TV Data Source)
        
        self.sitios.removeAll()
        self.tableView.reloadData()
        
        let iCloudContenedor = CKContainer.default()    // Contenedor basico en iCloud
        let publicDB = iCloudContenedor.publicCloudDatabase     // DataBase Publica
        let predicado = NSPredicate(value: true)    // Trae TODO el contenido (sin filtro)
        let miQuery = CKQuery(recordType: "Salas", predicate: predicado)  // Hace la consulta en el Record: Salas
        
        let queryOperacion = CKQueryOperation(query: miQuery)
        queryOperacion.desiredKeys = ["nombre","direccion","horario"]  // objetos puntuales que quiero traer
        queryOperacion.queuePriority = .veryHigh    // prioridad de traida
        queryOperacion.resultsLimit = 10    // limite de elementos
        
        queryOperacion.recordFetchedBlock = { (record: CKRecord?) in
            if let miRecord = record {
                self.sitios.append(miRecord)    // agregar cada elemento recuperado a la variable
            }
        }
        
        // le dice al completion block que hacer apenas termina de cargar, en este caso traer TODO al hilo de ejecucion principal
        queryOperacion.queryCompletionBlock = { (cursor: CKQueryOperation.Cursor?, error: Error?) in
            if error != nil {
                print(error as Any)
                return
            }
            
            OperationQueue.main.addOperation {      // Realiza la carga de datos en primer plano en lugar de en 2do
                self.spinner.stopAnimating()    // Detiene el spinner
                self.tableView.reloadData()
            }
        }
        
        publicDB.add(queryOperacion)    // ejecuta la peticion
        
        
        
    }
        
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.sitios.count
        //  return self.lugares!.count (CORE DATA)
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tableView.dequeueReusableCell(withIdentifier: "SalaCelda", for: indexPath) as! PlaceCell
        
        let sitio = self.sitios[indexPath.row]            // let lugar = self.lugares?[indexPath.row] (CORE DATA)

        if let nombre = sitio.object(forKey: "nombre") as? String {
            celda.namePlace?.text = nombre
        }
        
        if let direccion = sitio.object(forKey: "direccion") as? String {
            celda.locationPlace?.text = direccion
        }
        
        if let horario = sitio.object(forKey: "horario") as? String {
            celda.timePlace?.text = horario
        }
        
        // 2/2: Asignamos un "placeholder" a todos las UIImage y traemos las imagenes luego de cargados los Strings (en paralelo)
        
        celda.imagePlace?.image = #imageLiteral(resourceName: "placeholder")
    
        let publicDB = CKContainer.default().publicCloudDatabase
        let peticionOperacionImagen = CKFetchRecordsOperation(recordIDs: [sitio.recordID])
        peticionOperacionImagen.desiredKeys = ["imagen"]
        peticionOperacionImagen.queuePriority = .veryHigh
        
        peticionOperacionImagen.perRecordProgressBlock = { (record: CKRecord?, recordID: CKRecord.ID?, error: Error?) in
            if error != nil {
                print(error as Any)
                return
            }
        
            if let myRecord = record {
            OperationQueue.main.addOperation {
                    
                    if let imagen = myRecord.object(forKey: "imagen") {
                    let imagenAsset = imagen as! CKAsset
                        celda.imagePlace?.image = UIImage(data: NSData(contentsOf: imagenAsset.fileURL) as! Data)
                }
                    
            }
                
        }
            
            publidDB.add(peticionOperacionImagen)
            
        }
            /*  (CORE DATA)
                   celda.imagePlace.image = sitio.imagen
                   celda.namePlace.text = lugar?.nombre
                   celda.locationPlace.text = lugar?.direccion
                   celda.timePlace.text = lugar?.horario
            */
            
            // imagenes redondeadas
            celda.imagePlace.layer.cornerRadius = 10.0
            celda.imagePlace.clipsToBounds = true
            
        
        
        
        return celda
    }
                
        

    
    
    //MARK: - Compartir
    
    // Funcion para compartir, eliminar, etc
           
       override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
               
        // Compartir
               
            let shareAction = UIContextualAction(style: .normal, title: "Compartir") { (action, sourceView, completionHandler) in
               
         //       let lugar = self.lugares?[indexPath.row]
               
                let shareDefaultText = "Quiero compartirte ésta la sala de ensayo"
               
                let shareImage = self.lugares?[indexPath.row].imagen!
               
                let activityController = UIActivityViewController(activityItems: [shareDefaultText, shareImage as Any], applicationActivities: nil)
               
                   
                self.present(activityController, animated: true, completion: nil)
                }
           
                // color de fondo del boton COMPARTIR
                    shareAction.backgroundColor = UIColor(displayP3Red: 0.2, green: 0.2, blue: 0.9, alpha: 1.0)
               
        // Eliminar (CORE DATA)
               
            let deleteAction = UIContextualAction(style: .destructive, title: "Borrar") { (action, sourceView, completionHandler) in
                   
                if let contenedor = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer {
                let contexto = contenedor.viewContext
                    let lugarToBorrar = self.resultadosFetch.object(at: indexPath)
                    contexto.delete(lugarToBorrar)
                    
                    do {
                        try contexto.save()
                    } catch {
                        print("El error es el siguiente:")
                        print(error)
                    }
                }
                
                
            }
               
               // color de fondo del boton BORRAR
               deleteAction.backgroundColor = UIColor(displayP3Red: 0.9, green: 0.1, blue: 0.1, alpha: 1.0)
               
        
       
               // atributos en deslizar
               let swypeConfiguration = UISwipeActionsConfiguration(actions: [shareAction, deleteAction])
               
               return swypeConfiguration
    
               
       // Despues de configurar una celda, agregarla al UIController desde la parte grafica
       // al delegate y datos (2) arrastrandola.

       }
    

    @IBAction func unwindAlControladorNuevas (segue: UIStoryboardSegue) {
        
    }

}

// Modo Edicion

extension NuevasViewController : NSFetchedResultsControllerDelegate {
        
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            if let nuevoIndexPath = newIndexPath {
                self.tableView.insertRows(at: [nuevoIndexPath], with: .fade)
            }
            
        case .delete:
            if let chauIndexPath = indexPath {
                self.tableView.deleteRows(at: [chauIndexPath], with: .fade)
            }
            
        case .update:
            if let refrescoIndexPath = indexPath {
                self.tableView.reloadRows(at: [refrescoIndexPath], with: .fade)
            }
            
        case .move:
            if let indexPath = indexPath, let newIndexPath = newIndexPath {
                self.tableView.moveRow(at: indexPath, to: newIndexPath)
            }
        default:
            break
        }
        
        self.lugares = controller.fetchedObjects as? [Site]
        
        
    }
    
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
 
 */
        
}
